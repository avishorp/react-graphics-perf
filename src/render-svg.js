
import { useEffect, useState, useRef } from 'react';

export function RenderSVG(props) {
    const { width, height, data, selected, onSelect } = props;

    return <svg 
        width={width} 
        height={height}>
            { data.map((t, index) => 
                <path d={`M${t[0].x} ${t[0].y} L${t[1].x} ${t[1].y} L${t[2].x} ${t[2].y}z`} 
                style={{strokeWidth: 3, fill: index === selected? '#f00000' : '#404040'}}
                onMouseOver={() => onSelect(index)}
                onMouseOut={() => onSelect(null)} />
            )}
        </svg>;
}
