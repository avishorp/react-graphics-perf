import './App.css';
import { useState, useEffect } from 'react';
import { Slider, Switch } from '@material-ui/core';
import { RenderSVG } from './render-svg';
import { RenderCanvas } from './render-canvas';

const canvasWidth = 800;
const canvasHeight = 800;
const maxTriangleCount = 50000;

function genTriangle(width, height) {
  const maxRadius = 40;
  const minRadius = 10
  const vertices = [];
  const center = {
    x: Math.random()*width,
    y: Math.random()*height
  }

  let alpha = 0;
  for(let i = 0; i < 3; i++) {
    const r = Math.random()*(maxRadius - minRadius) + minRadius;
    alpha += Math.random()*Math.PI*120/180;
    vertices.push({
      x: center.x + r*Math.cos(alpha),
      y: center.y + r*Math.sin(alpha)
    });
  }
  
  return vertices;
}

const triangles = []

function App() {
  const [ svgMode, setSvgMode ] = useState(true);
  const [ objCount, setObjCount ] = useState(200);
  const [ selected, setSelected ] = useState(null);

  useEffect(() => {
    console.log('creating document');
    for(let i = 0; i < maxTriangleCount; i++)
      triangles.push(genTriangle(canvasWidth, canvasHeight));  
  }, [])

  const data = triangles.slice(0, objCount);

  return (
    <div>
      <div style={{ padding: '20px' }}>
      <div>Triangle count: {objCount}</div>
        <span>Canvas</span><Switch checked={svgMode} onChange={(ev, value) => setSvgMode(value)}/><span>SVG</span>
        <Slider
          value={objCount}
          min={200}
          max={maxTriangleCount}
          onChange={(ev, value) => { setObjCount(value) }}
        />
      </div>


      { svgMode? <RenderSVG width={canvasWidth} height={canvasHeight} data={data} selected={selected} onSelect={(n) => setSelected(n)}/>
        : <RenderCanvas width={canvasWidth} height={canvasHeight} data={data} selected={selected} onSelect={(n) => setSelected(n)}/> };
    </div>
  );
}

export default App;
