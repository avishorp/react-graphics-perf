
import { useEffect, useRef } from 'react';

export function RenderCanvas(props) {
    const { width, height, data, selected, onSelect } = props;

    const canvasRef = useRef(null);
  
    useEffect(() => {
        const canvas = canvasRef.current;
        if (canvas) {
          const context = canvas.getContext('2d');
    
          context.fillStyle = '#7fff7f';
          context.fillRect(0, 0, width, height);
             
          data.forEach((t, index) => {
            context.fillStyle = index === selected? '#f00000' : '#404040';

            context.beginPath();
            context.moveTo(t[0].x, t[0].y);
            context.lineTo(t[1].x, t[1].y);
            context.lineTo(t[2].x, t[2].y);
            context.closePath();
            context.fill();
        })
        }
      }, [ data, selected ]);

    const hits = data.map(t => {
        const p = new Path2D();
        p.moveTo(t[0].x, t[0].y);
        p.lineTo(t[1].x, t[1].y);
        p.lineTo(t[2].x, t[2].y);
        p.closePath();
        return p;
    });

    return <canvas ref={canvasRef} width={width} height={height} 
        onMouseMove={(ev) => {
            const canvas = canvasRef.current;
            if (canvas) {
                const x = ev.pageX - canvas.offsetLeft;
                const y = ev.pageY - canvas.offsetTop;

                const context = canvas.getContext('2d');
              for(let i = hits.length-1; i >= 0; i--) {
                  if (context.isPointInPath(hits[i], x, y)) {
                    onSelect(i);
                    return;
                  }

              }
              onSelect(null);
           }
        }
    }/>
}