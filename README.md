# SVG-vs-Canvas

This project is a sandbox for examining the performace of SVG vs. the performance of HTML Canvas.
It draws a picture, composed of multiple triangles. The amount of triangles can be adjusted using
a slider. When the mouse pointer is placed on one of the triangles, it changes its color from
grey to red.

The goal of the project is to implement the same logic both using SVG element and Canvas element and
to examine its performance differences. The differences can be observed when changing the underline data
(changing the number of trianges) and also in real-time, when one of the triangles has to change its color.

## Running
Run the application using `npm start`. Then browse to the local server address (a browser window or tab
should open automatically).

